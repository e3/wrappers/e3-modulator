require modbus
require essioc
require modulator

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "YURY-010:Ctrl-IOC-01")
epicsEnvSet(IOCDIR, "YURY-010_Ctrl-IOC-01")
epicsEnvSet(LOG_SERVER_NAME, "172.30.4.43")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# Load MODULATOR module
############################################################################
epicsEnvSet(SYS, "YURY")
epicsEnvSet(SUB, "010")
epicsEnvSet(ID, "001")
epicsEnvSet(MOD_IP, "172.18.156.59")
iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP)")

############################################################################
# IOC Startup
############################################################################
iocInit()

