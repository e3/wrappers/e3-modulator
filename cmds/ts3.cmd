require modbus
require essioc
require modulator

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "TS3-010:Ctrl-IOC-07")
epicsEnvSet(IOCDIR, "TS3-010_Ctrl-IOC-07")
epicsEnvSet(LOG_SERVER_NAME, "172.30.4.43")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# Load MODULATOR module
############################################################################
epicsEnvSet(SYS, "TS3")
epicsEnvSet(SUB, "010")
epicsEnvSet(ID, "007")
epicsEnvSet(MOD_IP, "172.30.5.106")
iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP)")

############################################################################
# Sequencer setup
############################################################################
epicsEnvSet(PREFIX, "$(SYS)-$(SUB):RFS-Mod-$(ID):")
#afterInit('seq modulator_seq "PREFIX=$(PREFIX)"') 

############################################################################
# IOC Startup
############################################################################
iocInit()

