require modbus
require essioc
require modulator

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "RFQ-010:Ctrl-IOC-03")
epicsEnvSet(IOCDIR, "RFQ-010_Ctrl-IOC-03")
epicsEnvSet(LOG_SERVER_NAME, "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# Load MODULATOR module
############################################################################
epicsEnvSet(SYS, "RFQ")
epicsEnvSet(SUB, "010")
epicsEnvSet(ID, "001")
epicsEnvSet(MOD_IP, "172.16.100.80")
epicsEnvSet(PSS_PV, "FEB-010Row:CnPw-U-004:ToRFQLPS")
iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP), PSS_PV=$(PSS_PV)")

############################################################################
# IOC Startup
############################################################################
iocInit()
