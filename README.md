
e3-modulator
======

European Spallation Source ERIC Site-specific EPICS module : modulator 

This module provides device support for the ESS High Voltage Modulators. 
The interface is based on modbus protocol and the IOC implements the basic operational commands of the device:
- Turn ON/OFF
- Set voltage amplitude and pulse length
- Reset interlocks

Note that this is an E3 "local" module that doesn't include another project as submodule.

Dependencies 
============ 
modbus
sequencer

Building the module 
=================== 
This module should compile as a regular E3 module. For instructions on how to build using E3 see the E3 documentation.
* Check if `configure/RELEASE` or `configure/RELEASE.local` reflects your E3 environment
* Run `make rebuild` from the command line.

