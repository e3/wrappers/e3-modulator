program modulator_seq

%% #include <string.h>
%% #include <stdio.h>
%% #include <math.h>
%% #include <stdarg.h>

%{#include <stdlib.h>}%
%{#include <unistd.h>}%

option +d;

#define OFF         0
#define STDBY       1
#define ON          2
#define STDBY_CMD   3
#define ON_CMD      4
#define STDBY_ILCK  5
#define ON_ILCK     6
#define INVALID     7
#define OFF_CMD     8

/********************* EPICS PVs declaration *************************/

/* Modulator state */
int modulator_state_pv;
assign modulator_state_pv to "{PREFIX}State-RB";
monitor modulator_state_pv;

/* Command PV */
int modulator_state_s_pv;
assign modulator_state_s_pv to "{PREFIX}State-Cmd";
monitor modulator_state_s_pv;
evflag writeFlag;
sync modulator_state_s_pv writeFlag;

/* SNL status PV */
int internal_rb_pv;
assign internal_rb_pv to "{PREFIX}InternalFSM";


/********************* Sequencer State Machine *************************/

ss modulator_ss {

	state init_state {

		entry {
            pvGet(modulator_state_pv);
            internal_rb_pv = INVALID;
            pvPut(internal_rb_pv);
		}

    	when (modulator_state_pv == OFF) {
            modulator_state_s_pv = OFF;
            pvPut(modulator_state_s_pv);
        }
		state modulator_off

    	when (modulator_state_pv == STDBY) {
            modulator_state_s_pv = STDBY;
            pvPut(modulator_state_s_pv);
        }
		state modulator_stdby

    	when (modulator_state_pv == ON) {
            modulator_state_s_pv = ON;
            pvPut(modulator_state_s_pv);
        }
		state modulator_on

		when(delay(10)) {}
		state init_state
	}

    state modulator_off {

        entry {
            pvGet(modulator_state_pv);
            internal_rb_pv = OFF;
            pvPut(internal_rb_pv);
            pvGet(modulator_state_s_pv);
        }

        when (modulator_state_pv == STDBY) {
            modulator_state_s_pv = STDBY;
            pvPut(modulator_state_s_pv);
        }
        state modulator_stdby

        // when (modulator_state_pv == ON) {
        //     modulator_state_s_pv = ON;
        //     pvPut(modulator_state_s_pv);
        // }
        // state modulator_on

        // when ((efTestAndClear( writeFlag )) && (modulator_state_s_pv == STDBY)) {}
        // state try_stdby

	    when ((efTestAndClear( writeFlag )) && (modulator_state_s_pv != OFF)) {}
        state try_stdby

		when(delay(10)) {}
		state modulator_off
    }

    state try_stdby {
        entry {
            pvGet(modulator_state_pv);
            pvGet(modulator_state_s_pv);
            internal_rb_pv = STDBY_CMD;
            pvPut(internal_rb_pv);
        }

        // wait 10 seconds to go to STDBY, otherwise reset
        when ((modulator_state_s_pv == STDBY) && (modulator_state_pv == STDBY)) {}
        state modulator_stdby

        // wait 10 seconds to go to ON, otherwise reset
        when ((modulator_state_s_pv == ON) && (modulator_state_pv == ON)) {}
        state modulator_on

		when(delay(30)) {}
		state check_interlock
    }

    state modulator_stdby {

        entry {
            pvGet(modulator_state_pv);
            internal_rb_pv = STDBY;
            pvPut(internal_rb_pv);
            pvGet(modulator_state_s_pv);
        }

        when (modulator_state_pv == ON) {
            modulator_state_s_pv = ON;
            pvPut(modulator_state_s_pv);
        }
        state modulator_on

        when (modulator_state_pv == OFF) {
            internal_rb_pv = STDBY_ILCK;
            pvPut(internal_rb_pv);
        }
        state check_interlock

        // Detect when trying to move to ON
        when ((efTestAndClear( writeFlag )) && (modulator_state_s_pv == ON)) {}
        state try_on

		when(delay(10)) {}
		state modulator_stdby
    }

    state try_on {
        entry {
            pvGet(modulator_state_pv);
            internal_rb_pv = ON_CMD;
            pvPut(internal_rb_pv);
            pvGet(modulator_state_s_pv);
        }

        // wait 10 seconds to go to ON, otherwise reset
        when (modulator_state_pv == ON) {}
        state modulator_on

		when(delay(30)) {}
		state check_interlock
    }

    state modulator_on {

        entry {
            pvGet(modulator_state_pv);
            pvGet(modulator_state_s_pv);
            internal_rb_pv = ON;
            pvPut(internal_rb_pv);
        }

        when ((efTestAndClear( writeFlag )) && (modulator_state_s_pv != ON)) {}
        state rollback

        when (modulator_state_pv != ON) {
            internal_rb_pv = ON_ILCK;
            pvPut(internal_rb_pv);
        }
        state check_interlock

		when(delay(10)) {}
		state modulator_on
    }

    state rollback {
        entry {
            pvGet(modulator_state_pv);
            pvGet(modulator_state_s_pv);
            if (modulator_state_s_pv == STDBY) {
                internal_rb_pv = STDBY_CMD;
            } else {
                internal_rb_pv = OFF_CMD;
            }
            pvPut(internal_rb_pv);
        }

        // wait 10 seconds to go to STDBY, otherwise reset
        when ((modulator_state_s_pv == STDBY) && (modulator_state_pv == STDBY)) {}
        state modulator_stdby

        // wait 10 seconds to go to ON, otherwise reset
        when ((modulator_state_s_pv == OFF) && (modulator_state_pv == OFF)) {}
        state modulator_off

		when(delay(10)) {}
		state check_interlock
    }


    state check_interlock {

        entry {
            pvGet(modulator_state_pv);
            pvGet(modulator_state_s_pv);
        }

        when (modulator_state_pv == STDBY) {
            modulator_state_s_pv = STDBY;
            pvPut(modulator_state_s_pv);
        }
        state modulator_stdby

        when (modulator_state_pv == OFF) {
            modulator_state_s_pv = OFF;
            pvPut(modulator_state_s_pv);
        }
        state modulator_off

        when (modulator_state_pv == ON) {
            modulator_state_s_pv = ON;
            pvPut(modulator_state_s_pv);
        }
        state modulator_on

		when(delay(10)) {}
		state check_interlock
    }

}

